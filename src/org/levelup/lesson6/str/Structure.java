package org.levelup.lesson6.str;

public interface Structure {

    // public static final String NAME = "";

    // public abstract
    void add(int value);

    void removeByIndex(int index);

}
