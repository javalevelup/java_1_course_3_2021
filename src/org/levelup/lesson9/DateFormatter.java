package org.levelup.lesson9;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class DateFormatter {

    // "2021-04-04" -> java.util.Date
    public Date parseString(String stringValue) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        try {
            Date date = formatter.parse(stringValue);
            System.out.println("Строка успешно сконвертировалась");
            return date;
        } catch (ParseException exc) {
            System.out.println("Ошибка при конвертации строки в дату: " + exc.getMessage());
        } catch (NullPointerException exc) {
            System.out.println("Произошло обращение к null-ссылке");
        } catch (Exception exc) {
            System.out.println("Что-то пошло не так");
        }
        return null;
    }

}
