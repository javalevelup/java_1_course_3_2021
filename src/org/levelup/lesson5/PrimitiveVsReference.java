package org.levelup.lesson5;

@SuppressWarnings("ALL")
public class PrimitiveVsReference {

    // all passed-by-value
    public static void main(String[] args) {
        int value = 10;                             // ac3
        Point point = new Point(10, 10);     // db5 -> x - db5.c1, y - db5.c3

        changePrimitiveValue(value);                // (10)
        changePointValue(point);                    // (db5)

        System.out.println("Primitive: " + value);  // ac3
        System.out.println("Point y value: " + point.y); // db5.c3

        value = changeAndReturnPrimitiveValue(value); // ac3 <- значение из ac7

    }

    static void changePrimitiveValue(int value) {   // ac6
        value = 20;                                 // ac6 <- 20
    }

    static int changeAndReturnPrimitiveValue(int value) { // ac7
        value = 20;                                       // ac7 <- 20
        return value;                                     // 20
    }

    static void changePointValue(Point point) {    // db5
        point.y = 20; // point.setY(20);           // db5.c3 <- 20
    }

}
