package org.levelup.lesson9;

// /src/org/levelup/lesson9/Stack.java
// /Users/dmitryprotsko/IdeaProjects/level-up/java_1_course_3_2021/src/org/levelup/lesson9/Stack.java
// LIFO - last in, first out
public class Stack {

    private int[] array;
    private int size;

    public Stack(int queueSize) {
        this.array = new int[queueSize];
    }

    public int take() {
        if (size == 0) {
            throw new EmptyStackException("Stack is empty");
        }
        return array[--size];
    }

    public void put(int val) throws StackOverflowException {
        if (array.length == size) {
            throw new StackOverflowException();
        }
        array[size++] = val;
    }

}
