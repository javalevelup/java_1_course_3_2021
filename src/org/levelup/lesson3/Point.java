package org.levelup.lesson3;

public class Point {

    int x; // class field/поле класса
    int y;

    Point() {
        this.x = 0;
        this.y = 0;
    }

    Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    void shiftX(int value) {
        x = x + value;
    }

    // Method signature - название + аргументы (порядок и типы и количество)
    // Overloading (перегрузка методов)
    void changePoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    void changePoint(Point other) {
        this.x = other.x;
        this.y = other.y;
    }

    // void m(int a, double b) - m(int, double)
    //
    // 1. void m(double b, int a) - m(double, int) - yes
    // 2. void m(int b, double a) - m(int, double) - no
    // 3. void m(int a, int b) - m(int, int) - yes
    // 4. void m(double a, double b) - m(double, double) - yes
    // 5. void m(double a, int b) - m(double, int) - yes
    // 6. int m(int a, double b) - m(int, double) - no
    // 7. int m(double a, int b) - m(double, int) - yes

}
