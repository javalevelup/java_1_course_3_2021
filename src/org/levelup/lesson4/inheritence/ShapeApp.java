package org.levelup.lesson4.inheritence;

@SuppressWarnings("ALL")
public class ShapeApp {

    public static void main(String[] args) {

        Shape shape = new Shape();
        System.out.println("Площадь фигуры: " + shape.calculateSquare());

        Triangle triangle = new Triangle(5, 5, 5);
        // System.out.println(triangle.toString());
        System.out.println("Площадь треугольника: " + triangle.calculateSquare());
        System.out.println("Равносторонний: " + triangle.isEquilateral());

        Triangle secondTriangle = new Triangle(6, 7, 9);
        RightTriangle rTriangle = new RightTriangle(3, 4, 5);

        Shape[] shapes = new Shape[] {
                triangle,
                secondTriangle,
                rTriangle
        };

        // shapes[0] = triangle; -> shapes[0] = (Shape) triangle;
        printPerimeters(shapes);

        printSquares(shapes);

//        int f = 0, s = 0, t = 0;
//        int[] array = new int[] { f, s, t };
//
//        int[] arr = new int[3];
//        arr[0] = f;
//        arr[1] = s;
//        arr[2] = t;

    }

    static void printPerimeters(Shape[] shapes) {
        for (int i = 0; i < shapes.length; i++) {
            System.out.println(shapes[i].calculatePerimeter());
        }
    }

    static void printSquares(Shape[] shapes) {
        for (int i = 0; i < shapes.length; i++) {
            // shapes[0] -> Triangle
            System.out.println(shapes[i].calculateSquare());
        }
    }

//    static void printPerimeters(Triangle[] figures) {
//        for (int i = 0; i < figures.length; i++) {
//            System.out.println(figures[i].calculatePerimeter());
//        }
//    }
//    static void printPerimeters(RightTriangle[] figures) {
//        for (int i = 0; i < figures.length; i++) {
//            System.out.println(figures[i].calculatePerimeter());
//        }
//    }
//    static void printPerimeters(Shape[] figures) {
//        for (int i = 0; i < figures.length; i++) {
//            System.out.println(figures[i].calculatePerimeter());
//        }
//    }
//    static void printPerimeters(Square[] figures) {
//        for (int i = 0; i < figures.length; i++) {
//            System.out.println(figures[i].calculatePerimeter());
//        }
//    }

}
