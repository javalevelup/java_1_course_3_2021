package org.levelup.lesson2;

@SuppressWarnings("ALL")
public class ArrayExample {

    public static void main(String[] args) {
        //
        double price1 = 534.34;
        double price2 = 432.12;
        double price3 = 594.32;
        double price4 = 345.54;

        double averagePrice = (price1 + price2 + price3 + price4) / 4;


        double[] prices = new double[4]; // массив цен типа double
        prices[0] = price1;
        prices[1] = price2;
        prices[2] = price3;
        prices[3] = price4;

        for (int i = 0; i < prices.length; i++) { // prices.length -> 4
            System.out.println(prices[i]);
        }

        double sum = 0;
        for (int i = 0; i < prices.length; i++) {
            sum += prices[i]; // sum = sum + prices[i];
            // +=, -=, /=, *=
        }
        double avg = sum / prices.length;
        System.out.println("Средняя цена: " + avg);

        String[] names = new String[3];
        names[0] = "name1";
        names[1] = "name2";
        names[2] = "name3";

        int[][] array2D = new int[3][4];


    }

}
