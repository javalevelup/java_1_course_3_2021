package org.levelup.lesson7;

public class App {

    public static void main(String[] args) {
        ObjectDynamicArray dynamicArray = new ObjectDynamicArray(3);
        dynamicArray.add(54);
        dynamicArray.add(45.3d);
        dynamicArray.add(new Object());
        dynamicArray.add("This is string");

        // String str = (String) dynamicArray.get(1); -> ClassCastException -> double cannot be cast to String

        GenericClass gc = new GenericClass("String");
        gc.setFieldValue(545.34);
        double val = (double) gc.getFieldValue();
        Double doubleRef = 545.434;

        GenericClass<Object> objectGC = new GenericClass<>(new Object());

        GenericClass<String> stringGC = new GenericClass<>("String"); // <> - diamond operator
        GenericClass<Integer> integerGC = new GenericClass<>(545);

        stringGC.setFieldValue("Only string");
        integerGC.setFieldValue(54);

        int v = integerGC.getFieldValue();
        String s = stringGC.getFieldValue();

    }

}
