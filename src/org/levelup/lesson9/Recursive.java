package org.levelup.lesson9;

public class Recursive {

    public static void main(String[] args) {
        int f = factorial(5);
        System.out.println(f);
    }

    // v = 5
    // factorial(5): return 5 * factorial(4) -> return 5 * 24; -> 120
    // factorial(4):     return 4 * factorial(3) -> return 4 * 6;
    // factorial(3):         return 3 * factorial(2) -> return 3 * 2;
    // factorial(2):             return 2 * factorial(1) -> return 2 * 1;
    // factorial(1):                 return 1;

    static int factorial(int value) {
        if (value <= 1) {
            return 1;
        }
        return value * factorial(value - 1);
    }

}
