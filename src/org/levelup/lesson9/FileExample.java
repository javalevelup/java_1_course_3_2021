package org.levelup.lesson9;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class FileExample {

    public static void main(String[] args) throws IOException {

        File universitiesFile = new File("universities.txt");   // not null
        File notExistentFile = new File("faculties.txt");       // not null

        System.out.println("File with universities: " + universitiesFile.exists());
        System.out.println("File with faculties: " + notExistentFile.exists());

        boolean result = notExistentFile.createNewFile();
        System.out.println("Creation result: " + result);

        // Input/Output Streams
        // InputStream
        // OutputStream
        // Reader
        // Writer

        // new Scanner(System.in);
        // System.in
        // System.out
        // System.err

        BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in)); // не надо вызывать метод close().
//        String line = consoleReader.readLine();
//        try {
//            Integer integer = Integer.valueOf(line);
//        } catch (NumberFormatException exc) {
//            System.out.println("Вы ввели не число!");
//        }


        BufferedReader fileReader = new BufferedReader(new FileReader(universitiesFile));
        String fileLine;
        while ( (fileLine = fileReader.readLine()) != null ) {
            System.out.println(fileLine);
        }
        fileReader.close();


        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(universitiesFile));
            String l;
            while ( (l = fileReader.readLine()) != null ) {
                System.out.println(l);
            }
        } catch (FileNotFoundException exc) {
            System.out.println(exc.getMessage());
        } catch (IOException exc) {
            exc.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException exc) {
                    System.out.println("Couldn't close file");
                }
            }
        }

    }

}
