package org.levelup.lesson4.inheritence;

// Triangle - subclass
// Shape - superclass (baseclass)
public class Triangle extends Shape {

    public Triangle() {
        // super(); // вызов конструктора (без параметров) базового класса
        System.out.println("Вызвали коструктор класса Triangle");
    }

    public Triangle(int first, int second, int third) {
        super(new int[] { first, second, third } );
    }

    public boolean isEquilateral() {
        return sizes[0] == sizes[1] && sizes[1] == sizes[2];
    }

    @Override
    protected double calculateSquare() {
        // ...
        // super.calculateSquare(); // вызов оригинального метода из родительского класса
        // ...

        double halfP = calculatePerimeter() / 2.0;
        return Math.sqrt(halfP * (halfP - sizes[0]) * (halfP - sizes[1]) * (halfP - sizes[2]));
    }

}
