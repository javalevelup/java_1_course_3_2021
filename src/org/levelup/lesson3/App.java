package org.levelup.lesson3;

public class App {

    // double variable;

    public static void main(String[] args) {

        // объект, экземпляр, ссылка, object, instance, reference
        Point p1 = new Point(); // 341
        // new
        //  1. Определение и резервирование памяти heap
        //  2. Получение ссылки на память
        p1.x = 54;
        p1.y = 21;
        System.out.println("Координаты точки p1: (" + p1.x + ", " + p1.y + ")");

        Point p2 = new Point(); // 123
        p2.x = 43;
        p2.y = 64;
        System.out.println("Координаты точки p2: (" + p2.x + ", " + p2.y + ")");

        Point p3 = new Point(); // 456
        p3.x = 10;
        p3.y = 15;

        Point p4 = new Point(); // 543
        p4.changePoint(52, 34);
        // p4.method(p2);

        Rectangle rec = new Rectangle(p1, p2, p3, p2);
        p3.shiftX(23);

        int perimeter = rec.calculatePerimeter();
        System.out.println("Периметр прямоугольника: " + perimeter);
        // System.out.println("Площадь прямоугольника: " + rec.calculateSquare());

        // rec.p1.x = 5;
        // int z;
        // Rectangle r = null;

        Rectangle rectangle = new Rectangle(
                new Point(4, 6),
                24,
                18
        );
        System.out.println("Периметр второго прямоугольника: " + rectangle.calculatePerimeter());
        // rectangle.rightTop = new Point()


    }

}
