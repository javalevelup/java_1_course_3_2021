package org.levelup.lesson8;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class MapExample {

    public static void main(String[] args) {

        // class Product { String name; Double price; }
        // Collection<Product> products = new ArrayList<>();

        Map<String, Double> productPrices = new HashMap<>(); // хэш-таблица

        productPrices.put("Asus VivoBook", 32990d);
        productPrices.put("HP 15s", 59715d);
        productPrices.put("Honor MagicBook", 59970d);
        productPrices.put("Apple MacBook 13", 98140d);
        productPrices.put("Lenovo IdeaPad", 68500d);

        Double price = productPrices.get("Honor MagicBook");
        Double value = productPrices.get("Laptop");

        System.out.println("1st price: " + price);
        System.out.println("2nd price: " + value);

        productPrices.put("Honor MagicBook", 68990d);
        // productPrices.replace("Honor MagicBook 2", 56965d);

        System.out.println();
        Set<String> keys = productPrices.keySet();
        for (String key : keys) {
            Double priceVal = productPrices.get(key);
            System.out.println("Key: " + key + ", value: " + priceVal);
        }

        // Entry - сущность пары ключ-значения
        // Map.Entry
        System.out.println();
        Set<Map.Entry<String, Double>> entries = productPrices.entrySet();
        for (Map.Entry<String, Double> entry : entries) {
            System.out.println("Key: " + entry.getKey() + ", value: " + entry.getValue());
        }

    }

}
