package org.levelup.universities.model;

import java.util.Objects;

@SuppressWarnings("ALL")
public class University {

    private String name;
    private String shortName;
    private int foundationYear;

    public University(String name, String shortName, int foundationYear) {
        this.name = name;
        this.shortName = shortName;
        this.foundationYear = foundationYear;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;

        // 2 variants of avoiding ClassCastException
        // 1var:
        //          instanceof
        //  <obj> instanceof <Class> -> вернет true, если объект имеет тип класса <Class> - если объект можно привести к типу <Class>
        //  null instanceof <any Class> -> false
        // if (!(object instanceof University)) return false;

        if (object == null || getClass() != object.getClass()) return false;

        University other = (University) object; // ClassCastException

        // return (name == other.name || (name != null && name.equals(other.name)))
        return Objects.equals(name, other.name) &&
                Objects.equals(shortName, other.shortName) &&
                foundationYear == other.foundationYear;
//        return name.equals(other.name) &&
//                shortName.equals(other.shortName) &&
//                foundationYear == other.foundationYear;
    }

    @Override
    public int hashCode() {
//        int result = 17;
//
//        result = 31 * result + name.hashCode();
//        result = 31 * result + shortName.hashCode();
//        result = 31 * result + foundationYear;
//
//        return foundationYear;
        return Objects.hash(name, shortName, foundationYear);
    }

    @Override
    public String toString() {
        return "University{" +
                "name='" + name + '\'' +
                ", shortName='" + shortName + '\'' +
                ", foundationYear=" + foundationYear +
                '}';
    }

}
