package org.levelup.lesson7;

public class ObjectDynamicArray {

    private Object[] elements;
    private int size;

    public ObjectDynamicArray(int initialSize) {
        this.elements = new Object[initialSize]; // [0, 0, 0]
    }

    protected void add(Object value) {
        if (elements.length == size) {
            Object[] oldArray = elements;
            elements = new Object[(int)(size * 1.5)];
            System.arraycopy(oldArray, 0, elements, 0, oldArray.length);
        }
        elements[size++] = value;
    }

}
