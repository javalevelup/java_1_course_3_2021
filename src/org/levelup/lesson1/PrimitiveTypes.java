package org.levelup.lesson1;

// org.levelup.lesson1.PrimitiveTypes - полное имя класса
public class PrimitiveTypes {

    public static void main(String[] args) {

        //
        /* */

        // <тип переменной> название_переменной
        int a; // объявили переменную a
        a = 34582; // присвоили значение 34582 переменной a.

        int b = 198323; // инициализация переменной

        int sum = a + b;
        System.out.println(sum);
        System.out.println("Сумма = " + sum); // "Сумма = " + 232905 -> "Сумма = " + "232905" -> "Сумма = 232905"

        // System.out.println("Разность = " + b - a);
        System.out.println("Разность = " + (b - a));

        int first = 10;
        int second = 10;
        System.out.println(first + second + " = sum"); // 20 = sum
        System.out.println("sum = " + first + second); // sum = 1010

        int var = 20;
        // var = var + 1;
        var++; // ~ var = var + 1
        var--; // ~ var = var - 1

        // var - 20
        // prefix increment  - ++var - сначала увеличить значение на 1, потом все остальные операции
        // postfix increment - var++ - сначала выполнить остальные операции, потом увеличить на 1
        // ++var;
        // var++;

        System.out.println("Var1 = " + var++); // Var1 = 20
        System.out.println("Var2 = " + ++var); // Var2 = 22

        // System.out.println("Var1 = " + var);
        // var = var + 1
        // var = var + 1
        // System.out.println("Var2 = " + var);

        int d = 43;
        int j = 4;
        System.out.println("Остаток от деления = " + (d % j));

    }

}
