package org.levelup.lesson3;

// Модификитор доступа (поля, методы, конструкторы, классы)
// private - доступ только внутри класса (доступ только внутри фигурных скобок класса)
// default-package (private-package) - доступ внутри класса и внутри пакета
// protected - default-package + доступ в классах-наследниках
// public - доступ отовсюду
@SuppressWarnings("ALL")
public class Rectangle {

    // Point[] points;

    private Point leftBottom;
    private Point rightBottom;
    private Point leftTop;
    private Point rightTop;

    Rectangle(Point np1, Point np2, Point np3, Point np4) {
        leftBottom = np1;
        rightBottom = np2;
        leftTop = np3;
        rightTop = np4;
    }

    Rectangle(Point point, int width, int length) {
        leftBottom = point; // left bottom
        rightBottom = new Point(leftBottom.x + width, leftBottom.y); // right bottom
        leftTop = new Point(leftBottom.x, leftBottom.y + length); // left top
        rightTop = new Point(leftBottom.x + width, leftBottom.y + length); // right top
    }

    // <тип возвращаемого значения>/void <название метода>(arg_type1 arg_name1, arg_type2 arg_name2,) {}
    int calculatePerimeter() {
        int wight = Math.abs(rightTop.x - leftTop.x);
        int length = Math.abs(leftTop.y - leftBottom.y);

        // int perimeter = 2 * width + 2 * length;
        // return perimeter;

        return 2 * wight + 2 * length;
    }

    private double calculateSquare() {
        return 0;
    }

}
