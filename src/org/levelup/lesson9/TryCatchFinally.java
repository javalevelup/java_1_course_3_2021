package org.levelup.lesson9;

@SuppressWarnings("ALL")
public class TryCatchFinally {

    public static void main(String[] args) {
        int result = method(10); // 3
        System.out.println(result);
    }

    static int method(int i) {
        try {
            System.out.println("try-block");
            if (i == 10) {
                throw new RuntimeException();
            }
            return 1;
        } catch (Exception exc) {
            System.out.println("catch-block");
            return 2;
        } finally {
            System.out.println("finally-block");
            return 3;
        }
    }

    static void doNotInvokeFinally() {
        try {
            System.out.println("try-block");
            // System.exit(0);
            // while (true) {} // for (;;)
        } catch (Throwable exc) {
            System.out.println("catch-block");
        } finally {
            System.out.println("finally-block");
        }
    }

}
