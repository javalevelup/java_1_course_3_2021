package org.levelup.universities.menu;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConsoleMenu {

    // Static field vs Usual field
    // 1. Имеет всегда одно значение в единицу времени
    // 2. Обращение к этому полю происходит через имя класса (а не через объект)
    private static final BufferedReader CONSOLE_READER = new BufferedReader(new InputStreamReader(System.in));

    public void printMenu() {
        System.out.println("1. Добавить университет");
        System.out.println("2. Удалить университет");
        System.out.println("3. Вывести список университетов");
        System.out.println("0. Выход из программы");
    }

    public int enterCommand() throws IOException {
        System.out.println("Введите номер команды:");
        String command = CONSOLE_READER.readLine();
        return Integer.parseInt(command);
    }

}
