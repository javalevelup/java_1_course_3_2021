package org.levelup.universities.menu.command;

import org.levelup.universities.menu.Command;
import org.levelup.universities.model.University;
import org.levelup.universities.service.UniversityService;

import java.util.List;

public class PrintUniversitiesCommand implements Command {

    private UniversityService universityService;

    public PrintUniversitiesCommand() {
        this.universityService = new UniversityService();
    }

    @Override
    public void executeCommand() {
        System.out.println("Список университетов:");
        List<University> universities = universityService.list();
        for (University university : universities) {
            System.out.println(university);
        }
        System.out.println();
    }

}
