package org.levelup.universities.service;

import org.levelup.universities.model.University;

import java.util.ArrayList;
import java.util.List;

public class UniversityService {

    private static final String UNIVERSITIES_FILENAME = "universities.txt";

    private FileService fileService;

    public UniversityService() {
        this.fileService = new FileService();
    }

    public List<University> list() {
        List<String> lines = fileService.readAllLines(UNIVERSITIES_FILENAME);
        List<University> universities = new ArrayList<>();
        for (String line : lines) {
            // СПбГУ;Государственный Университет;1809
            // split(;) -> String[] { "СПбГУ", "Государственный Университет", "1809"  }
            String[] values = line.split(";");
            universities.add(new University(values[1], values[0], Integer.parseInt(values[2])));
        }
        return universities;
    }

}
