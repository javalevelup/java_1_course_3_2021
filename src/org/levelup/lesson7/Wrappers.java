package org.levelup.lesson7;

@SuppressWarnings("ALL")
public class Wrappers {

    public static void main(String[] args) {

        Integer f = 5634;
        Integer t = 5634;
        // f == t -> false
        // f.equals(t) -> true

        Integer r = null;
        Boolean b = null;

        // boxing - primitive -> wrapper (int -> Integer)
        // unboxing - wrapper -> primitive (Integer -> int)
        // autoboxing/autounboxing
        int intValue = 564;
        // Integer integerValue = Integer.valueOf(intValue);
        Integer integerValue = intValue;
        // int fromWrapper = integerValue.intValue();
        int fromWrapper = integerValue;

        // DB
        // int valueFromDB = db.getInt();  -> NPE
        // null.intValue() -> NPE

        int v = Integer.valueOf("565");
    }

}
