package org.levelup.lesson9;

public class StackApp {

    public static void main(String[] args) { // throws StackOverflowException {
        Stack stack = new Stack(3);
        // int res = stack.take();
        try {
            stack.put(34);
            stack.put(34);
            stack.put(34);
            stack.put(34);
        } catch (StackOverflowException exc) {
            exc.printStackTrace();
            System.out.println(exc.getMessage());
            // throw new RuntimeException();
        }
        System.out.println("After try-catch");
    }

    // Thread.run();

}
