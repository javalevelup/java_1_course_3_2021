package org.levelup.lesson7;

public class GenericClass<T> {

    private T fieldValue; // after compilation -> private Object fieldValue;

    public GenericClass(T fieldValue) {
        this.fieldValue = fieldValue;
    }

    public T getFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(T fieldValue) {
        this.fieldValue = fieldValue;
    }

}
