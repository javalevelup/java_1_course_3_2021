package org.levelup.lesson4.inheritence;

import java.util.Arrays;

@SuppressWarnings("ALL")
public class Shape {

    protected int[] sizes;

    public Shape() {
        System.out.println("Вызвали коструктор класса Shape");
    }

    public Shape(int[] sizes) {
        this.sizes = sizes;
    }

    protected int calculatePerimeter() {
        int perimeter = 0;

        for (int i = 0; i < sizes.length; i++) {
            perimeter += sizes[i];
        }

        return perimeter;
    }

    protected double calculateSquare() {
        return 0;
    }

//    @Override
//    public String toString() {
//        return "Shape{" +
//                "sizes=" + Arrays.toString(sizes) +
//                '}';
//    }
}
