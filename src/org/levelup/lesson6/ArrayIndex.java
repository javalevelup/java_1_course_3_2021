package org.levelup.lesson6;

public class ArrayIndex {

    public static void main(String[] args) {
        int[] array = { 5, 6, 7, 8, 9, 9};
        for (int i = 0; i < array.length; i++) {
            int el = array[i];
            System.out.println(el);
        }
    }

}
