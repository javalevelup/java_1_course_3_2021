package org.levelup.lesson4.inheritence;

@SuppressWarnings("ALL")
public class TypeCast {

    public static void main(String[] args) {

        RightTriangle rt = new RightTriangle(3, 4, 5);
        Triangle triangle = new RightTriangle(3, 4, 5);

        Shape shapeRT = rt;
        Object objectShapeRT = shapeRT;
        Object objectRT = rt;

        // shapeRT.isEquiateral();

        RightTriangle rtFromObject = (RightTriangle) objectRT;
//        Triangle triangle = (Triangle) objectRT;
//
        Triangle tr = new Triangle();
        Object o = tr;
        RightTriangle rtFromTriangle = (RightTriangle) o;
    }

}
