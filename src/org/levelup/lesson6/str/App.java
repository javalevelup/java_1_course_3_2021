package org.levelup.lesson6.str;

public class App {

    public static void main(String[] args) {
        OneLinkedList<Integer> oll = new OneLinkedList<>(); // oll.head = null
        oll.add(40);
        oll.add(13);
        oll.add(66);
        oll.add(45);
        oll.add(98);
        oll.add(12);
        oll.print();


        // AbstractStructure as = new AbstractStructure();
        // as.isEmpty(); -> всегда возвращается false.
        int[] arr = { 4, 6, 7 };
        System.out.println(arr);

        DynamicArray dynamicArray = new DynamicArray(3);
        System.out.println("Количество элементов в динамическом массиве: " + dynamicArray.size());
        // dynamicArray.....();

        dynamicArray.add(4);
        dynamicArray.add(5);
        dynamicArray.add(8);
        dynamicArray.add(3);
        dynamicArray.add(4);

        // AbstractStructure as = new DynamicArray();
        // AbstractStructure as = dynamicArray;
        // as.add();
    }

}
