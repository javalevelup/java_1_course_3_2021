package org.levelup.lesson9;

import java.util.Date;

public class DateFormatterApp {

    public static void main(String[] args) {
        DateFormatter f = new DateFormatter();
        Date date = f.parseString("12.08.2021 22:30:23");
        if (date != null) {
            System.out.println(date.getTime());
        }
    }

}
