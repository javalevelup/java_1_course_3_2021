package org.levelup.universities.menu;

import org.levelup.universities.menu.command.AddUniversityCommand;
import org.levelup.universities.menu.command.PrintUniversitiesCommand;

import java.util.HashMap;
import java.util.Map;

// public abstract class CommandFactory { - хороший вариант, когда мы хотим от него в дальнейшем унаследоваться
public final class CommandFactory {

    private CommandFactory() {}

    private static Map<Integer, Command> commands = new HashMap<>();

    static {
        commands.put(1, new AddUniversityCommand());
        commands.put(3, new PrintUniversitiesCommand());
    }

//    {
//
//    }
//
//    {
//
//    }

    public static Command findCommand(int commandNumber) {
        // usualMethod(); < нельзя!
        // anotherStaticMethod(); < можно!
        // usualField = 3; < нельзя!
        // staticField = 6; < можно!
        return commands.get(commandNumber);
    }

    // public void usualMethod() {}
    // public static anotherStaticMethod() {}
    // private int usualField;
    // private static int staticField;

}
