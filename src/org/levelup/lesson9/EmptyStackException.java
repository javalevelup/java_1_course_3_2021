package org.levelup.lesson9;

public class EmptyStackException extends RuntimeException {

    public EmptyStackException(String message) {
        super(message);
    }

    public EmptyStackException(String message, Throwable cause) {
        super(message, cause);
    }

}
