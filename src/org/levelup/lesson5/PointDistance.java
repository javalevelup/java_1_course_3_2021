package org.levelup.lesson5;

// PointService, PointManager, PointComponent
public class PointDistance {

    public double distance(Point f, Point s) {
        double k1 = Math.abs(f.getX() - s.getX());
        double k2 = Math.abs(f.getY() - s.getY());
        // g = sqrt(k1 * k1 + k2 * k2)
        return Math.sqrt(k1 * k1 + k2 * k2);
    }

    public int quadrant(Point point) {
        if (point.getX() >= 0 && point.getY() >= 0) {
            return 1;
        } else if (point.getX() > 0 && point.getY() < 0) {
            return 2;
        } else if (point.getX() < 0 && point.getY() < 0) {
            return 3;
        } else {
            return 4;
        }
    }

    // public double distance(int x1, int y1, int x2, int y2);
    // public int quadrant(int x, int y)


}
