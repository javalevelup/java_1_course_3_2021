package org.levelup.lesson6.str;

// 1. Нельзя создать объект абстрактного класса
// 2. АК может содержать абстрактные методы

// Абстрактный метод - метод, который не имеет тела
// Абстрактный метод - описание метода (описание сигнатуры метода)
public abstract class AbstractStructure<T> {

    protected int size;

    // Всегда добавление происходит в конец
    protected abstract void add(T value);

    protected abstract T get(int index);

    protected abstract void removeByIndex(int index);

    protected boolean isEmpty() {
        return size == 0;
    }

    protected int size() { // количество элементов в структуре
        return size;
    }

}
