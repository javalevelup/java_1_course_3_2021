package org.levelup.lesson6.str;

// "Динамический" массив - массив, которые умеет изменять размер
// Список на основе массива - список - набор однотипных элементов

// Если мы добавляем новый элемент в заполненный массив, то массив увеличится в размере
// Новые значения мы добавляем в конец массив - значения хранятся в порядке добавления
// 0. [0, 0, 0]
// 1. add(4) -> [4, 0, 0]
// 2. add(6) -> [4, 6, 0]
// 3. add(1) -> [4, 6, 1]
// 4. add(3) -> [4, 6, 1, 3]
// 5. add(9) -> [4, 6, 1, 3, 9, 0]
public class DynamicArray<E> extends AbstractStructure<E> {

    private Object[] elements;

    public DynamicArray(int initialSize) {
        this.elements = new Object[initialSize]; // [0, 0, 0]
    }

    // 0. [0, 0, 0], size = 0
    // 1. add(0) -> [0, 0, 0], size = 1
    // 2. add(0) -> [0, 0, 0], size = 2
    // 3. add(1) -> [4, 6, 1]
    // 4. add(3) -> [4, 6, 1, 3]
    // 5. add(9) -> [4, 6, 1, 3, 9, 0]
    @Override
    protected void add(E value) {
        // DynamicArray<String> array = new DynamicArray<>();
        // array.add(""); <- only String
        if (elements.length == size) {
            // Увеличиваем размер массива в 1.5 раза
            Object[] oldArray = elements;
            elements = new Object[(int)(size * 1.5)];
            // Скопировать значения из oldArray в elements
            System.arraycopy(oldArray, 0, elements, 0, oldArray.length);
        }
        // Место в массиве есть

        // 0. size = 0, [0, 0, 0]
        // 1. add(4) -> elements[0] = 4, size = 1 - [4, 0, 0]
        // 2. add(6) -> elements[1] = 6, size = 2

        // elements[size] = value;
        // size = size + 1;
        elements[size++] = value;
    }

    @Override
    protected E get(int index) {
        return (E) elements[index];
    }

    @Override
    protected void removeByIndex(int index) {

    }

}
