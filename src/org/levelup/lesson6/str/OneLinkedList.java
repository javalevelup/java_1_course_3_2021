package org.levelup.lesson6.str;

// OneLinkedList - связный список
// public class OneLinkedList<T> extends AbstractStructure<T> {
// <E extends ...> -> E extends Number -> только класс Number или его наследников
// <E super ...> -> E super Number
// OneLinedList<DynamicArray> l = new OneLinkedList<>();

// OneLinkedList<String> list = new OneLinkedList<>();
//  Generic type - java.lang.String
public class OneLinkedList<E> extends AbstractStructure<E> {

    private Element<E> head; // первый элемент в связном списке

    @Override
    public void add(E value) { // void add(T value) -> void add(Object value)
        Element<E> el = new Element<>(value); // Element<String> el = new Element<>(value);
        if (head == null) {
            // Список пуст
            head = el;
        } else {
            Element<E> cursor = head; // current
            while (cursor.next != null) {
                cursor = cursor.next;
            }
            // Как только cursor.next станет равным null, то cursor будет указывать на последний элемент
            cursor.next = el;
        }
        size++;
    }

    @Override
    protected E get(int index) {
        return null;
    }

    @Override
    public void removeByIndex(int index) {

    }

    public boolean isCyclic() {
        return false;
    }

    public void print() {
        Element<E> cursor = head;
        while (cursor.next != null) {
            System.out.println(cursor.value);
            cursor = cursor.next;
        }
    }

}
