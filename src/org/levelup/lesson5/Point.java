package org.levelup.lesson5;

// anemic domain model
// rich domain model
public class Point {

    private int x;
    int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    // getters/setters

    // getter
    // public <тип поля> get<Имя поля>() {}
    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    // setter
    // public void set<Имя поля>(<Тип поля> <имя поля>) {}
    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int quadrant() {
        if (x >= 0 && y >= 0) {
            return 1;
        } else if (x > 0 && y < 0) {
            return 2;
        } else if (x < 0 && y < 0) {
            return 3;
        } else {
            return 4;
        }
    }

    public double distance(Point s) {
        double k1 = Math.abs(this.x - s.x);
        double k2 = Math.abs(this.y - s.y);
        // g = sqrt(k1 * k1 + k2 * k2)
        return Math.sqrt(k1 * k1 + k2 * k2);
    }
    
}
