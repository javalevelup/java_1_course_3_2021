package org.levelup.lesson9;

// checked exception
public class StackOverflowException extends Exception {

    public StackOverflowException() {
        super("Stack is full");
    }

}
