package org.levelup.lesson1;

import java.util.Scanner;

public class ConsoleReader {

    public static void main(String[] args) {
        // Как считать значения с консоли (ввести значения руками)

        Scanner sc = new Scanner(System.in);
        System.out.println("Введите число:");
        int number = sc.nextInt();
        System.out.println("Вы ввели число " + number);
    }

}
