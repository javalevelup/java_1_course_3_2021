package org.levelup.universities.menu;

public interface Command {

    void executeCommand();

}
