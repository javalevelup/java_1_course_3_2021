package org.levelup.universities.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileService {

    public List<String> readAllLines(String filepath) {
        List<String> lines = new ArrayList<>();
        // try-with-resources

        try (BufferedReader reader = new BufferedReader(new FileReader(filepath))) {
            String line;
            while ((line = reader.readLine()) != null) {
                // trim() -> удаляет пробелы вначале строки и вконце -> "   4345 fvwf  ".trim() -> "4345 fvwf"
                if (!line.trim().isEmpty()) { // если строка не пуста
                    lines.add(line);
                }
            }
            return lines;
        } catch (IOException exc) {
            System.out.println("Ошибка чтения файла: " + exc.getMessage());
            return new ArrayList<>(); // возвращаем пустой список, если не смогли дочитать файл до конца
        }
    }

    public void addNewLines(String filepath, List<String> lines) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filepath, true))) {
            for (String line : lines) {
                // write.write(line + "\n");
                writer.newLine(); // более правильный вариант
                writer.write(line);
            }
            writer.flush();
        } catch (IOException exc) {
            System.out.println("Ошибка при записи в файл '" + filepath + "': " + exc.getMessage());
        }
    }

}
