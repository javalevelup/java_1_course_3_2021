package org.levelup.universities.app;

import org.levelup.universities.menu.Command;
import org.levelup.universities.menu.CommandFactory;
import org.levelup.universities.menu.ConsoleMenu;

import java.io.BufferedReader;
import java.io.IOException;

public class UniversitiesApplication {

    public static void main(String[] args) {
        System.out.println("Программа по работе с базой университетов");
        // BufferedReader consoleReader = ConsoleMenu.consoleReader;

        ConsoleMenu consoleMenu = new ConsoleMenu();
        consoleMenu.printMenu();

        int command;
        do {
            try {
                command = consoleMenu.enterCommand();
                Command commandExecutor = CommandFactory.findCommand(command);
                if (commandExecutor != null) {
                    commandExecutor.executeCommand();
                } else if (command != 0) {
                    System.out.println("Команды с таким номером не существует");
                }
            } catch (IOException | NumberFormatException exc) {
                System.out.println("Вы ввели неправильный номер команды");
                command = -1;
            }
        } while (command != 0);

        System.out.println("Пока!");
    }

}
