package org.levelup.lesson6.str;

class Element<V> {

    V value;
    Element<V> next;

    Element(V value) {
        this.value = value;
        this.next = null;
    }

}
