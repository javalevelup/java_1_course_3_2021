package org.levelup.lesson4.inheritence;

public class RightTriangle extends Triangle {

    public RightTriangle(int first, int second, int third) {
        super(first, second, third);
    }

    @Override
    public boolean isEquilateral() {
        return false;
    }

}
