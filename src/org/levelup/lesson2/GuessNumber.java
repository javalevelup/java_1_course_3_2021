package org.levelup.lesson2;

import java.util.Scanner;

public class GuessNumber {

    public static void main(String[] args) {

        int secret = 10;

        Scanner sc = new Scanner(System.in);
        System.out.println("Введите число: "); // sout
        int enteredNumber = sc.nextInt();

//        if (enteredNumber == secret) {
//            // Если enteredNumber равен secret, то выполнится эта часть кода
//            System.out.println("Вы угадали!");
//            // int localVariable = 13;
//        } else {
//            System.out.println("Вы ввели неправильное число!");
//            System.out.println("Загаданное число: " + secret);
//        }

        while (enteredNumber != secret) {
            System.out.println("Вы ввели неправильное число!");
            System.out.println("Введите число заново: ");
            enteredNumber = sc.nextInt();
        }

        System.out.println("Вы угадали!");
        System.out.println("Программа завершилась...");

    }

}
