package org.levelup.lesson2;

public class Loops {

    public static void main(String[] args) {
        for (int i = 0; i < 5; i++) {
            System.out.println("i value = " + i);
            if (i == 3) {
                break;
            }
        }

        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 10; j++) {
                System.out.print(j + " ");
                if (j == 4) {
                    break;
                }
            }
            System.out.println();
        }

        System.out.println();

        // for (;;) {}
        int i = 0;
        do {
            System.out.println("Do..while");
            i++;
        } while (i < 5);

        System.out.println("Конец программы");
    }

}
