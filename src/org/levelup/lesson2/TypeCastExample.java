package org.levelup.lesson2;

@SuppressWarnings("ALL")
public class TypeCastExample {

    // psvm
    public static void main(String[] args) {
        int intVar = 503;
        long longVar = intVar; // приведение типов - неявное расширяющее преобразование

        byte byteVar = (byte) longVar; // явное сужающее преобразование
        double doubleVar = intVar;

        // 0.1 + 0.2 = (!=) 0.3
        // 0.1 + 0.2 ~ 0.2999999999999999999999999999999999999999999999999999999999
        // 0.1 + 0.2 ~ 0.3000000000000000000000000000000000000000000000000000000001
        // 0.3 = 0.30000000000000000000000000000000000000000000000000000

    }

}
