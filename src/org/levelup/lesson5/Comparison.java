package org.levelup.lesson5;

import org.levelup.universities.model.University;

@SuppressWarnings("ALL")
public class Comparison {

    public static void main(String[] args) {
        University u1 = new University("Политех", "СПбГПУ", 1893);
        University u2 = new University("Политех", "СПбГПУ", 1893);

        // University u3 = u1;
        // u1 == u3
        boolean isEqual = u1 == u2;
        boolean areObjectsEqual = u1.equals(u2);

        System.out.println("Are objects equal: " + areObjectsEqual);

        University u3 = null;
        boolean equalsWithNull = u1.equals(u3);

        boolean equalsWithAnotherType = u1.equals(new Point(3, 45));

        // u1.equals(u2) возвращает true, то u2.equals(u1) тоже возвращает true

        if (u1.hashCode() == u2.hashCode() && u1.equals(u2)) {
            System.out.println("Объекты одинаковы!");
        }

    }

    // class A {}, class B extends A {}
    // B b = new B();
    // b instanceof A -> true
    // A a = new A();

    // b.equals(a) -> false, a.equals(b) -> true

    // b instanceof A -> true
    // a instanceof B -> false

}
