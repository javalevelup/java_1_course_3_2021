package org.levelup.lesson8;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class CollectionExamples {

    public static void main(String[] args) {
        // Collection<>
        // Set<> / List<>

        List<String> names = new LinkedList<>();
        names.add("Dmitry");
        names.add("Andrey");
        names.add("Kirill");
        names.add("Konstantin");
        names.add("Svetlana");
        names.add("Tatiana");
        names.add("Dmitry");

        String forthIndex = names.get(4);  // Set не имеет метода get(int index)
        System.out.println(forthIndex);

        boolean isIgorExist = names.contains("Igor"); // поиск объекта происходит по equals
        System.out.println("Is exist: " + isIgorExist);

        int index = names.indexOf("Igor"); // names.lastIndexOf() - indexOf/lastIndexOf - это методы List
        System.out.println("Igor's index: " + index);
        System.out.println("Dmitry's index: " + names.indexOf("Dmitry"));
        System.out.println("Last Dmitry's index: " + names.lastIndexOf("Dmitry"));

        names.remove("Dmitry");
        System.out.println("New first element: " + names.get(0));

        List<String> subList = names.subList(3, 6); // элемента под индексом 6 не будет - (3, 4, 5)

        List<Integer> integers = new ArrayList<>();
        integers.add(2);
        integers.add(3);
        integers.add(4);
        integers.add(5);
        integers.add(6);

        Integer valueForRemoval = 6;
        // integers.remove(Integer.valueOf(6));
        integers.remove(valueForRemoval);
        // integers.remove(6);
        // integers.remove(6); << here will be "remove by index"


        // foreach - сокращенный for
        // for (int i = 0; i < array.length; i++) {}
        // List<String> names;
        // for (<GenericType/ArrayType> <var> : <collectionObject/arrayObject>)
        System.out.println();
        for (String name : names) {
            // 1 iteration: "Andrey"
            // 2 iteration: "Kirill"
            // ...
            System.out.println(name);
            // if (name.equals("Dmitry")) { ... } -> ConcurrentModificationException
        }

        List<String> filtered = new ArrayList<>();
        for (String name : names) {
            if (!name.equals("Dmitry")) {
                filtered.add(name);
            }
        }
        System.out.println();
        System.out.println("Filtered collection:");
        for (String name : filtered) {
            System.out.println(name);
        }

        Iterator<String> iter = names.iterator();
        while (iter.hasNext()) {
            String name = iter.next();
            System.out.println(name);
        }

    }

}
